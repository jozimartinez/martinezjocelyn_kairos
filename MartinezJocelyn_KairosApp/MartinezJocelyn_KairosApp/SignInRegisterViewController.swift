//
//  SignInRegisterViewController.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/6/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//
//  Background Image: https://messengerinternational.org/blog/podcast/live-fearlessly/
//  Logo Placeholder Image: http://pngimage.net/logo-placeholder-png/

import UIKit
import Firebase

class SignInRegisterViewController: UIViewController{
    
    // MARK: ERROR STRINGS
    enum TextFieldError: String {
        case firstName = "Please enter your first name."
        case lastName = "Please enter your last name."
        case email = "Please enter an email."
        case password = "The password must be 6 characters long or more."
    }
    
    // MARK: - ELEMENTS
    let backgroundImage: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "Background Image"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    //Exit view
    let cancelButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.roundedRect)
        button.setTitle("Cancel", for: UIControl.State.normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(17.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.black, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(cancel), for: UIControl.Event.touchUpInside)
        button.isUserInteractionEnabled = true
        return button
    }()

    //Display the church logo
    let logoContainer: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "Logo Placeholder"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    //Display a message
    let messageContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        //messageContainer.backgroundColor = UIColor.white.withAlphaComponent(0.8)
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()
    
    let signMessage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Register to see the latest events"
        label.font = label.font.withSize(17.0)
        label.textAlignment = .center
        label.textColor = UIColor.black
        return label
    }()
    
    //Displays the inputs the user will use to enter info
    let inputContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        //view.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        return view
    }()

    let firstNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "First Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        //textField.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    let textFieldSeperatorOne: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        return view
    }()
    
    let lastNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Last Name"
        textField.translatesAutoresizingMaskIntoConstraints = false
        //textField.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    let textFieldSeperatorTwo: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        return view
    }()
    let emailTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Email"
        textField.keyboardType = UIKeyboardType.emailAddress
        //textField.backgroundColor = UIColor.white
        textField.translatesAutoresizingMaskIntoConstraints = false
        //textField.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    let textFieldSeperatorThree: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.black.withAlphaComponent(0.2)
        return view
    }()
    
    let passwordTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Password"
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.isSecureTextEntry = true
        //textField.addTarget(self, action: #selector(textFieldDidChange), for: UIControl.Event.editingChanged)
        return textField
    }()
    
    let logInRegisterButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        button.setTitle("Sign Up", for: UIControl.State.normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(UIColor.white, for: UIControl.State.normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.addTarget(self, action: #selector(logInOrRegister), for: UIControl.Event.touchUpInside)
        return button
    }()
    
  lazy var loginRegisterSegmentedControl: UISegmentedControl = {
        let segmentControl = UISegmentedControl(items: ["Sign In", "Sign Up"])
        segmentControl.translatesAutoresizingMaskIntoConstraints = false
        segmentControl.tintColor = UIColor.black.withAlphaComponent(0.8)
        segmentControl.backgroundColor = UIColor.white
        segmentControl.selectedSegmentIndex = 1
        segmentControl.addTarget(self, action: #selector(handleSegmentValueChange), for: UIControl.Event.valueChanged)
     
        return segmentControl
    }()
    
    // MARK: - CONSTRAINT VARIABLES
    var inputContainerHeight: NSLayoutConstraint?
    var firstNameHeight: NSLayoutConstraint?
    var lastNameHeight: NSLayoutConstraint?
    var separatorOneHeight: NSLayoutConstraint?
    var separatorTwoHeight: NSLayoutConstraint?
    var emailHeight: NSLayoutConstraint?
    var passwordHeight: NSLayoutConstraint?
    
     var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addConstraints()
        
       
    }
    
    // MARK: - SEGMENTED CONTROL FUNCTION
    
    @objc func handleSegmentValueChange(){
        let buttonTitle = loginRegisterSegmentedControl.titleForSegment(at: loginRegisterSegmentedControl.selectedSegmentIndex)
        logInRegisterButton.setTitle(buttonTitle, for: UIControl.State.normal)
        self.signInRegisterConstraints()
        
    }
    
    // MARK: - CANCEL/DISMISS VIEW FUNCTION
    @objc func cancel(){
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - SIGN UP OR IN FUNCTION
    @objc func logInOrRegister(){
        if loginRegisterSegmentedControl.selectedSegmentIndex == 0 {
            signInHandle()
        }else{
            signUpHandle()
        }
        
        if let presenter = presentedViewController as? AccountViewController {
            presenter.displayLogin = false
        }
    }

    func signInHandle(){
        
        guard let email = emailTextField.text, let password = passwordTextField.text else {
            return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let codeError = AuthErrorCode(rawValue: error!._code){
                switch codeError {
                case .missingEmail: print("Missing email")
                case .invalidEmail: print("invalid email")
                case .wrongPassword: print("wrong password")
                default: print("Sign in user error: {0}", error!)
                }
                 return
            }
              self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    func signUpHandle(){
        
        guard let firstName = firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), let lastName = lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines), let password = passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            
            return
        }
        
        if !firstName.isEmpty && !lastName.isEmpty && !email.isEmpty && !password.isEmpty{
            Auth.auth().createUser(withEmail: email, password: password) { (User, addingError) in
                if addingError != nil{
                   if let codeError = AuthErrorCode(rawValue: addingError!._code){
                    
                    if codeError == .emailAlreadyInUse {
                        
                    } else if codeError == .invalidEmail {
                        self.emailTextField.attributedPlaceholder = self.placeHolder(message: addingError!.localizedDescription)
                    }
                    
                    if codeError == .weakPassword {
                        self.passwordTextField.attributedPlaceholder = self.placeHolder(message: addingError!.localizedDescription)
                    }
                   
                }
                    return
                }
                
                let values = ["email": email, "firstName" : firstName, "lastName": lastName ]
                
                self.ref = Database.database().reference(fromURL: "https://kairos-68358.firebaseio.com/").child("users")
                self.ref.updateChildValues(values, withCompletionBlock: { (error, ref) in
                    if(error != nil){
                        print("Error Occur: {0}", error.debugDescription)
                        return
                    }
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            
        } else {
        
            firstNameTextField.attributedPlaceholder = placeHolder(message: TextFieldError.firstName.rawValue)
            lastNameTextField.attributedPlaceholder = placeHolder(message: TextFieldError.lastName.rawValue)
            emailTextField.attributedPlaceholder = placeHolder(message: TextFieldError.email.rawValue)
            passwordTextField.attributedPlaceholder = placeHolder(message: TextFieldError.password.rawValue)
        }
       
    }
    
    func placeHolder(message: String) -> NSAttributedString? {
       var placeHolder = NSMutableAttributedString()
        
        placeHolder = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 15)])
        if message == TextFieldError.password.rawValue{
            placeHolder = NSMutableAttributedString(string: message, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 13)])
        }
        placeHolder.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.red.withAlphaComponent(0.8), range: NSRange(location: 0, length: message.count))
    
        return placeHolder
    }

}


