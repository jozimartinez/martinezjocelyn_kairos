//  Jocelyn Martinez
//  MDV 5 - 1810
//  
//  AppDelegate.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 9/30/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import Firebase
import WatchConnectivity

//Icons from: https://www.flaticon.com/

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    /*
    var session: WCSession? {
        didSet{
            if let session = session {
                session.delegate = self
                session.activate()
            }
        }
    }
 
 */
    //Images
    let homeIcon = UIImage(named: "Home Icon")
    let profileIcon = UIImage(named: "Profile Icon")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        setUpTabController()
       
        
      /*
        
        if WCSession.isSupported() {
            session = WCSession.default
        }
        */
        return true
    }
    
    // MARK: - TAB VIEW CONTROLLER SET UP
    func setUpTabController(){
        let tabBarController = UITabBarController()
        let homeView = HomeViewController()
        let navigationController = AccountViewController()
        let accountView = UINavigationController(rootViewController: navigationController)
        
        if let homeIcon = homeIcon, let profileIcon = profileIcon {
            let firstTabBarItem = UITabBarItem(title: "", image: homeIcon, tag: 0)
            let secondTabBarItem = UITabBarItem(title: "", image: profileIcon, tag: 1)
            homeView.tabBarItem = firstTabBarItem
            accountView.tabBarItem = secondTabBarItem
        }
        let controllers = [homeView, accountView ]
        tabBarController.viewControllers = controllers
        window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
    }

}

/*

extension AppDelegate: WCSessionDelegate {
    //Required protocols
    func sessionDidDeactivate(_ session: WCSession) {
    }
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {

    }
    
    //This will send back the custom object array created
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        DispatchQueue.main.async {
            if(message["getUser"] as? Bool) != nil {
                NSKeyedArchiver.setClassName("User", for: User.self)
                
                
                let userValue: User = User(firstName: "Jane", lastName: "Doe", email: "JanDoe@gmail.com")
                guard let data = try? NSKeyedArchiver.archivedData(withRootObject: userValue as User, requiringSecureCoding: false) as NSData else{
                    fatalError("Can't encode data")}
                
                 replyHandler(["newUser":data])
            }
        }
    }
/*
    func retrieveCurrentUser() -> User{
        var user = User(firstName: "Jane", lastName: "Doe", email: "JanDoe@gmail.com")
        if Auth.auth().currentUser?.uid != nil {
            let uid = Auth.auth().currentUser?.uid
            let ref = Database.database().reference(withPath: "https://kairos-68358.firebaseio.com/")
            
            ref.child("users").child(uid!).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                if let userDictionary = snapshot.value as? [String: AnyObject] {
                    
                    user = User(firstName: (userDictionary["firstName"] as? String)!, lastName: (userDictionary["lastName"] as? String)!, email: (userDictionary["email"] as? String)!)
                    print(user)
                }
            }, withCancel: nil)
        }
        
        return user
    }
 */
}

 */
