//
//  HomeViewController.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/3/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//
//  Religious logos: https://www.graphicsprings.com/category/religious
import UIKit
import Firebase



class HomeViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    // MARK: - ELEMENTS
    let logoImage: UIImageView = {
        let imageView = UIImageView(image: UIImage(named: "Logo"))
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let sundaytitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Sunday\n3:00PM"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let sundayService: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Celebration"
        label.font = label.font.withSize(15)
        label.textColor = UIColor.orange
        return label
    }()
    
    let thursdaytitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Thursday\n7:00PM"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let thursdayService: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 2
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Worship and\ndiscipleship"
        label.font = label.font.withSize(15)
        label.textColor = UIColor.orange
        return label
    }()
    
    let fridaytitle: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Friday\n7:00PM"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    let fridayService: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Youth Ministry"
        label.font = label.font.withSize(15)
        label.textColor = UIColor.orange
        return label
    }()
 
    let serviceViewContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        //view.backgroundColor = UIColor.blue
        return view
    }()
    
    let messageViewContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor(red: 255/256, green: 175/256, blue: 58/256, alpha: 1.0)
        return view
    }()
    
    let homeMessage: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.text = "It's time for Christ"
        label.font = UIFont(name: "Avenir", size: 40.0)
        label.font = label.font.withSize(40.0)
        return label
    }()
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.white
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled = false
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell1")
        collectionView.register(EventsCollectionViewCell.self, forCellWithReuseIdentifier: "cell2")
        return collectionView
    }()
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    let churchImages = ["Find Christ", "Love Christ", "Give Christ", "Read Christ"]
    var events = [Event]()
    
    var signedIn: Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Auth.auth().currentUser?.uid != nil {
            signedIn = true
            let uid = Auth.auth().currentUser?.uid
            print(uid!)
            //Need to work on accessing the database
            Database.database().reference(fromURL: "https://kairos-68358.firebaseio.com/").child("users").child(uid!).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                print("inside")
                if (snapshot.value as? [String:Any]) != nil{
                    print("Did let dictionary")
                }else{
                    print("Didn't let dictionary")
                }
                
            }, withCancel: { (error) in
                print(error)
            })
        }else{
            signedIn = nil
        }
        
        events.append(contentsOf: Event.eventsToDisplay(signedIn: signedIn))
        
        print(events.count)
        self.addConstraints()
        
        self.view.backgroundColor = UIColor.white
        self.tabBarController?.tabBar.tintColor = UIColor.orange
    }
  
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return (section == 0) ? churchImages.count : events.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell1", for: indexPath)
            let imageView: UIImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 148, height: 180))
            imageView.image = UIImage(named: churchImages[indexPath.row])
            cell.contentView.addSubview(imageView)
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell2", for: indexPath) as! EventsCollectionViewCell
            cell.backgroundColor = UIColor.blue
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 0 {
            return UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 30)
            //layout.itemSize = CGSize(width: 148, height: 180)
        }else{
            return UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 30)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: 148, height: 180)
        }else{
            return CGSize(width: collectionView.bounds.size.width, height: 180)
        }
        
    }

}
