//
//  AccountViewController.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/5/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import UIKit
import Firebase

class AccountViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
   

    var person: User!
    
    // MARK: - ELEMENTS
    let inputContainer: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(red: 255/256, green: 175/256, blue: 58/256, alpha: 1.0)
        return view
    }()
    
    let loginLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(17.0)
        label.text = "Sign in to view your events."
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.roundedRect)
        button.setTitle("Login or Register", for: UIControl.State.normal)
        button.titleLabel?.font = button.titleLabel?.font.withSize(14.0)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.orange, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(displaySignInRegisterView), for: UIControl.Event.touchUpInside)
        return button
    }()
    
    let displayName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var collectionView: UICollectionView = {
        let collectionLayout = UICollectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: collectionLayout)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.isScrollEnabled = false
        return collectionView
    }()
    
    var signedIn: Bool? = false
    var displayLogin: Bool = false
    
    // MARK: - CONSTRAINT VARIABLES
    var inputContainerHeight: NSLayoutConstraint?
    var labelHeight: NSLayoutConstraint?
    var buttonHeight:NSLayoutConstraint?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if Auth.auth().currentUser?.uid != nil {
            print("User is signed in")
            signedIn = true
            displayLogin = true
            self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(title: "Sign Out", style: UIBarButtonItem.Style.plain, target: self, action: #selector(signOutHandler))
            
            let uid = Auth.auth().currentUser?.uid
            print(uid!)
            //Need to work on accessing the database
            Database.database().reference(fromURL: "https://kairos-68358.firebaseio.com/").child("users").child(uid!).observeSingleEvent(of: DataEventType.value, with: { (snapshot) in
                print("inside")
                if (snapshot.value as? [String:Any]) != nil{
                    print("Did let dictionary")
                }else{
                    print("Didn't let dictionary")
                }
                
            }, withCancel: { (error) in
                print(error)
            })
        }else{
             print("User is not signed in")
            signedIn = nil
            displayLogin = false
            addConstraints()
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.topItem?.title = "Account"
        self.navigationController?.navigationBar.tintColor = UIColor.orange
        self.view.backgroundColor = UIColor.white
   
    }
    
    @objc func displaySignInRegisterView(){
        self.navigationController?.present(SignInRegisterViewController(), animated: true, completion: nil)
    }
    
    @objc func signOutHandler(){
        
        do{
            try Auth.auth().signOut()
            self.navigationController?.navigationBar.topItem?.rightBarButtonItems = []
        }catch let logOutError {
            print(logOutError)
        }
       
        displayInput()
    }
    
    func displayInput(){
        inputContainerHeight?.isActive = false
        inputContainerHeight = inputContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.15)
        inputContainerHeight?.isActive = true
        labelHeight?.isActive = false
        labelHeight = loginLabel.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 0.5)
        labelHeight?.isActive = true
        buttonHeight?.isActive = false
        buttonHeight = loginButton.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 0.3)
        buttonHeight?.isActive = true
    }
    
    func removeInput(){
        inputContainerHeight?.isActive = false
        inputContainerHeight = inputContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.8)
        inputContainerHeight?.isActive = true
        labelHeight?.isActive = false
        labelHeight = loginLabel.heightAnchor.constraint(equalToConstant: 0)
        labelHeight?.isActive = true
        buttonHeight?.isActive = false
        buttonHeight = loginButton.heightAnchor.constraint(equalToConstant: 0)
        buttonHeight?.isActive = true
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (person.events?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        cell.backgroundColor = UIColor.blue
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 30, bottom: 10, right: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width, height: 180)
    }
}
