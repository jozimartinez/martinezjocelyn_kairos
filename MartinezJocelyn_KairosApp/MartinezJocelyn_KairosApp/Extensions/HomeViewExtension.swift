//
//  HomeViewExtension.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/15/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import Foundation
import UIKit

extension HomeViewController{
    
    func addConstraints() {
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //let screenHeight = screenSize.height
        scrollView.contentSize = CGSize(width: screenWidth, height: 1200)
        self.view.addSubview(scrollView)
        
        scrollView.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
       
        scrollView.addSubview(logoImage)
        logoImage.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        logoImage.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 20).isActive = true
        logoImage.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.55).isActive = true
        logoImage.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 0.25).isActive = true
        
        scrollView.addSubview(serviceViewContainer)
        serviceViewContainer.topAnchor.constraint(equalTo: logoImage.bottomAnchor, constant: 10).isActive = true
        serviceViewContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        serviceViewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: 0.95).isActive = true
        serviceViewContainer.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 0.20).isActive = true
        
        serviceViewContainer.addSubview(thursdaytitle)
        thursdaytitle.centerXAnchor.constraint(equalTo: serviceViewContainer.centerXAnchor).isActive = true
        thursdaytitle.centerYAnchor.constraint(equalTo: serviceViewContainer.centerYAnchor, constant: -40).isActive = true
        
        thursdaytitle.widthAnchor.constraint(equalTo: serviceViewContainer.widthAnchor, multiplier: 1/3).isActive = true
        thursdaytitle.heightAnchor.constraint(equalTo: serviceViewContainer.heightAnchor, multiplier: 0.4).isActive = true
        
        serviceViewContainer.addSubview(thursdayService)
        thursdayService.centerXAnchor.constraint(equalTo: serviceViewContainer.centerXAnchor).isActive = true
        thursdayService.topAnchor.constraint(equalTo: thursdaytitle.bottomAnchor, constant: 2).isActive = true
        thursdayService.heightAnchor.constraint(equalTo: serviceViewContainer.heightAnchor, multiplier: 1/3).isActive = true
        thursdayService.widthAnchor.constraint(equalTo: thursdaytitle.widthAnchor, multiplier: 1.0).isActive = true
        
        serviceViewContainer.addSubview(sundaytitle)
        sundaytitle.centerYAnchor.constraint(equalTo: thursdaytitle.centerYAnchor).isActive = true
        sundaytitle.rightAnchor.constraint(equalTo: thursdaytitle.leftAnchor).isActive = true
        sundaytitle.widthAnchor.constraint(equalTo: thursdaytitle.widthAnchor, multiplier: 1.0).isActive = true
        sundaytitle.heightAnchor.constraint(equalTo: thursdaytitle.heightAnchor, multiplier: 1.0).isActive = true
        
        serviceViewContainer.addSubview(sundayService)
       
        sundayService.rightAnchor.constraint(equalTo: thursdayService.leftAnchor).isActive = true
        sundayService.topAnchor.constraint(equalTo: sundaytitle.bottomAnchor, constant: 0).isActive = true
        sundayService.heightAnchor.constraint(equalTo: thursdayService.heightAnchor, multiplier: 1.0).isActive = true
        sundayService.widthAnchor.constraint(equalTo: thursdayService.widthAnchor, multiplier: 1.0).isActive = true
        
        
        serviceViewContainer.addSubview(fridaytitle)
         fridaytitle.leftAnchor.constraint(equalTo: thursdaytitle.rightAnchor).isActive = true
        fridaytitle.centerYAnchor.constraint(equalTo: thursdaytitle.centerYAnchor).isActive = true
        fridaytitle.widthAnchor.constraint(equalTo: thursdaytitle.widthAnchor, multiplier: 1.0).isActive = true
        fridaytitle.heightAnchor.constraint(equalTo: thursdaytitle.heightAnchor, multiplier: 1.0).isActive = true
        
        serviceViewContainer.addSubview(fridayService)
        
        fridayService.leftAnchor.constraint(equalTo: thursdayService.rightAnchor).isActive = true
        fridayService.topAnchor.constraint(equalTo: fridaytitle.bottomAnchor, constant: 0).isActive = true
        fridayService.heightAnchor.constraint(equalTo: thursdayService.heightAnchor, multiplier: 1.0).isActive = true
        fridayService.widthAnchor.constraint(equalTo: thursdayService.widthAnchor, multiplier: 1.0).isActive = true
        
        scrollView.addSubview(messageViewContainer)
        messageViewContainer.topAnchor.constraint(equalTo: thursdayService.bottomAnchor, constant: 10).isActive = true
        messageViewContainer.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        messageViewContainer.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        messageViewContainer.heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        messageViewContainer.addSubview(homeMessage)
        homeMessage.topAnchor.constraint(equalTo: messageViewContainer.topAnchor).isActive = true
        homeMessage.leftAnchor.constraint(equalTo: messageViewContainer.leftAnchor).isActive = true
        homeMessage.rightAnchor.constraint(equalTo: messageViewContainer.rightAnchor).isActive = true
        homeMessage.bottomAnchor.constraint(equalTo: messageViewContainer.bottomAnchor).isActive = true
        
        scrollView.addSubview(collectionView)
        collectionView.topAnchor.constraint(equalTo: messageViewContainer.bottomAnchor).isActive = true
        collectionView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        collectionView.heightAnchor.constraint(equalTo: scrollView.heightAnchor, multiplier: 1.0).isActive = true
       
        
    }
}
