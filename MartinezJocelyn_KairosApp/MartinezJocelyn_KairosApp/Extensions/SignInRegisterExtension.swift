//
//  SignInRegisterExtension.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/6/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import Foundation
import UIKit

extension SignInRegisterViewController {
    
    func addConstraints(){
        
        self.view.addSubview(backgroundImage)
        backgroundImage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        backgroundImage.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        backgroundImage.heightAnchor.constraint(equalTo: self.view.heightAnchor).isActive = true
        backgroundImage.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        
        //Adding the button to dismiss the view controller and constraints
        self.view.addSubview(cancelButton)
        
        cancelButton.topAnchor.constraint(equalTo: backgroundImage.layoutMarginsGuide.topAnchor).isActive = true
        cancelButton.rightAnchor.constraint(equalTo: backgroundImage.layoutMarginsGuide.rightAnchor).isActive = true
        
        self.view.addSubview(inputContainer)
        inputContainer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        inputContainer.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        inputContainer.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.9).isActive = true
        
        inputContainerHeight = inputContainer.heightAnchor.constraint(equalToConstant: 203)
        inputContainerHeight?.isActive = true
        
        inputContainerElements()
        
        self.view.addSubview(logInRegisterButton)
        logInRegisterButton.topAnchor.constraint(equalTo: inputContainer.bottomAnchor, constant: 10).isActive = true
        logInRegisterButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        logInRegisterButton.widthAnchor.constraint(equalTo: inputContainer.widthAnchor, multiplier: 1.0).isActive = true
        logInRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.view.addSubview(loginRegisterSegmentedControl)
        loginRegisterSegmentedControl.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        loginRegisterSegmentedControl.bottomAnchor.constraint(equalTo: inputContainer.topAnchor, constant: -12).isActive = true
        loginRegisterSegmentedControl.widthAnchor.constraint(equalTo: inputContainer.widthAnchor, multiplier: 1.0).isActive = true
        loginRegisterSegmentedControl.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    func inputContainerElements(){
        //
        inputContainer.addSubview(firstNameTextField)
        firstNameTextField.leftAnchor.constraint(equalTo: inputContainer.leftAnchor, constant: 12).isActive = true
        firstNameTextField.topAnchor.constraint(equalTo: inputContainer.topAnchor).isActive = true
        firstNameTextField.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        
        firstNameHeight = firstNameTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 1/4)
        firstNameHeight?.isActive = true
        
        //
        inputContainer.addSubview(textFieldSeperatorOne)
        textFieldSeperatorOne.leftAnchor.constraint(equalTo: inputContainer.leftAnchor).isActive = true
        textFieldSeperatorOne.topAnchor.constraint(equalTo: firstNameTextField.bottomAnchor).isActive = true
        textFieldSeperatorOne.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        separatorOneHeight = textFieldSeperatorOne.heightAnchor.constraint(equalToConstant: 1)
        separatorOneHeight?.isActive = true
        
        //
        inputContainer.addSubview(lastNameTextField)
        
        lastNameTextField.leftAnchor.constraint(equalTo: inputContainer.leftAnchor, constant: 12).isActive = true
        lastNameTextField.topAnchor.constraint(equalTo: textFieldSeperatorOne.bottomAnchor).isActive = true
        lastNameTextField.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        lastNameHeight = lastNameTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 1/4)
        lastNameHeight?.isActive = true
        
        //
        inputContainer.addSubview(textFieldSeperatorTwo)
        textFieldSeperatorTwo.leftAnchor.constraint(equalTo: inputContainer.leftAnchor).isActive = true
        textFieldSeperatorTwo.topAnchor.constraint(equalTo: lastNameTextField.bottomAnchor).isActive = true
        textFieldSeperatorTwo.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        separatorTwoHeight = textFieldSeperatorTwo.heightAnchor.constraint(equalToConstant: 1)
            separatorTwoHeight?.isActive = true
        
        //
        inputContainer.addSubview(emailTextField)
        emailTextField.leftAnchor.constraint(equalTo: inputContainer.leftAnchor, constant: 12).isActive = true
        emailTextField.topAnchor.constraint(equalTo: textFieldSeperatorTwo.bottomAnchor).isActive = true
        emailTextField.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        
        emailHeight = emailTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 1/4)
        emailHeight?.isActive = true
        
        //
        inputContainer.addSubview(textFieldSeperatorThree)
        
        textFieldSeperatorThree.leftAnchor.constraint(equalTo: inputContainer.leftAnchor).isActive = true
         textFieldSeperatorThree.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
         textFieldSeperatorThree.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
         textFieldSeperatorThree.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        //
        inputContainer.addSubview(passwordTextField)
        
        passwordTextField.leftAnchor.constraint(equalTo: inputContainer.leftAnchor, constant: 12).isActive = true
        passwordTextField.topAnchor.constraint(equalTo: textFieldSeperatorThree.bottomAnchor).isActive = true
        passwordTextField.widthAnchor.constraint(equalTo: inputContainer.widthAnchor).isActive = true
        
        passwordHeight = passwordTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: 1/4)
        passwordHeight?.isActive = true
    }
    
    func signInRegisterConstraints(){
        inputContainerHeight?.constant = loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 100 : 200
        firstNameHeight?.isActive = false
        firstNameHeight = firstNameTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/4)
        firstNameHeight?.isActive = true
        
        lastNameHeight?.isActive = false
        lastNameHeight = lastNameTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1/4)
        lastNameHeight?.isActive = true
        
        emailHeight?.isActive = false
        emailHeight = emailTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/4)
        emailHeight?.isActive = true
        
        passwordHeight?.isActive = false
        passwordHeight = passwordTextField.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 1/2 : 1/4)
        passwordHeight?.isActive = true
        
        separatorOneHeight?.isActive = false
        separatorOneHeight = textFieldSeperatorOne.heightAnchor.constraint(equalToConstant: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1)
        separatorOneHeight?.isActive = true
        
        separatorTwoHeight?.isActive = false
        separatorTwoHeight = textFieldSeperatorTwo.heightAnchor.constraint(equalToConstant: loginRegisterSegmentedControl.selectedSegmentIndex == 0 ? 0 : 1)
        separatorTwoHeight?.isActive = true
    }

}
