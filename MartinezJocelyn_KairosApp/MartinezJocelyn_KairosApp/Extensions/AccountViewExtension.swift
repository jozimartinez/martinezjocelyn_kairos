//
//  AccountViewExtension.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/9/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import Foundation
import UIKit

extension AccountViewController {
    
    func addConstraints(){
        //Adding the input container and constraints
        self.view.addSubview(inputContainer)
        inputContainer.topAnchor.constraint(equalTo: self.view.layoutMarginsGuide.topAnchor, constant: (displayLogin == false) ? 10 : 0).isActive = true
        inputContainer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        inputContainer.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: (displayLogin == false) ? 0.95 : 1.0).isActive = true
        
        inputContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: (displayLogin == false) ?  0.15 : 1.0).isActive = true
        
        //Adding the label and constraints
        inputContainer.addSubview(loginLabel)
        
        loginLabel.topAnchor.constraint(equalTo: inputContainer.topAnchor, constant: 0).isActive = true
        loginLabel.leftAnchor.constraint(equalTo: inputContainer.leftAnchor, constant: 0).isActive = true
        loginLabel.rightAnchor.constraint(equalTo: inputContainer.rightAnchor, constant: 0).isActive = true
        
        loginLabel.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: (displayLogin == false) ? 0.5 : 0).isActive = true
        
        //Adding the button
        inputContainer.addSubview(loginButton)
        loginButton.topAnchor.constraint(equalTo: loginLabel.bottomAnchor).isActive = true
        loginButton.centerXAnchor.constraint(equalTo: loginLabel.centerXAnchor).isActive = true
        loginButton.widthAnchor.constraint(equalTo: inputContainer.widthAnchor, multiplier: (displayLogin == false) ? 0.7 : 0).isActive = true
        
        loginButton.heightAnchor.constraint(equalTo: inputContainer.heightAnchor, multiplier: (displayLogin == false) ? 0.3: 0).isActive = true
        
        
    }
}
