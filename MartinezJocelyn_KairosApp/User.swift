//
//  User.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/9/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import Foundation
class User: NSObject, NSCoding {
    
    var firstName: String?
    var lastName: String?
    var email: String?
    var events: [Event]?
    
    init(firstName: String, lastName: String, email: String){
        self.firstName = firstName
        self.lastName = lastName
        self.email = email
        self.events = [Event]()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(firstName: "Jocelyn", lastName: "Martinez", email: "J@gmail.com")
        firstName = aDecoder.decodeObject(forKey:"firstNameKey") as? String
        lastName = aDecoder.decodeObject(forKey: "lastNameKey") as? String
        email = aDecoder.decodeObject(forKey: "emailKey") as? String
        events = aDecoder.decodeObject(forKey: "eventsKey") as? [Event]
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstNameKey")
        aCoder.encode(lastName, forKey: "lastNameKey")
        aCoder.encode(email, forKey: "emailKey")
        aCoder.encode(events, forKey: "eventsKey")
    }
    
    
}
