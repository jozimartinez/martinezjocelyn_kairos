//
//  Event.swift
//  MartinezJocelyn_KairosApp
//
//  Created by Jocelyn Martinez Hernandez on 10/18/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import Foundation
class Event: NSObject, NSCoding {
    var eventTitle: String?
    var eventDescription: String?
    var eventDate: String?
    var attending: Bool?
    
    init(eventTitle: String, eventDescription: String, eventDate: String, attending: Bool?){
        self.eventTitle = eventTitle
        self.eventDescription = eventDescription
        self.eventDate = eventDate
        self.attending = attending
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(eventTitle: "Prayers for Jesus", eventDescription: "We will be praying to all those in need in the name of Jesus.", eventDate: "October 28 at 5:00 p.m.", attending: nil)
        
        eventTitle = aDecoder.decodeObject(forKey:"eventTitleKey") as? String
        eventDescription = aDecoder.decodeObject(forKey: "eventDescriptionKey") as? String
        eventDate = aDecoder.decodeObject(forKey: "eventDateKey") as? String
        attending = aDecoder.decodeObject(forKey: "attendingKey") as? Bool
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(eventTitle, forKey: "eventTitleKey")
        aCoder.encode(eventDescription, forKey: "eventDescriptionKey")
        aCoder.encode(eventDate, forKey: "eventDateKey")
        aCoder.encode(attending, forKey: "attendingKey")
    }
    
    public static func eventsToDisplay(signedIn: Bool?) -> [Event]{
        return [Event(eventTitle: "Let Jesus Take The Wheel", eventDescription: "We will be celebrating the newcomers that have decided to give their lives to Christ.", eventDate: "October 26, 2018 at 5:00 p.m", attending: signedIn), Event(eventTitle: "It's A Miracle!", eventDescription: "We will be gathering to celebrate to Jesus for all the things we are thankful for.", eventDate: "October 30, 2018 at 2:00 p.m", attending: signedIn)]
    }
    
    func eventsAdded(signedIn: Bool?) -> [Event]{
        return [Event(eventTitle: "Prayers for Rachel", eventDescription: "Sister Rachel and her family have been going through a rought time. Come gather in prayer and ask Jesus to help her.", eventDate: "September 2, 2018 at 3:00 p.m", attending: signedIn), Event(eventTitle: "Christmas Prayers", eventDescription: "Get together as a huge family to celebrate the birth of Jesus.", eventDate: "December 25, 2018 at 4:00 p.m", attending: signedIn)]
    }
}
