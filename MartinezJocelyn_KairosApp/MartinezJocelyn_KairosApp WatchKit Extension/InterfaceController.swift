//
//  InterfaceController.swift
//  MartinezJocelyn_KairosApp WatchKit Extension
//
//  Created by Jocelyn Martinez Hernandez on 9/30/18.
//  Copyright © 2018 Full Sail University. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController , WCSessionDelegate{
    @IBOutlet weak var notSignedIn: WKInterfaceLabel!
    
     @available(watchOS 2.2, *)
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        let userValues: [String: Any] = ["getUser": true]
        if session.isReachable{

            session.sendMessage(userValues, replyHandler: { (replyData) in
                
                if let data = replyData["newUser"] as? Data {
                    
                    print("Inside Send Message")
                    NSKeyedUnarchiver.setClass(User.self, forClassName: "User")
                    do{
                        guard let userData = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? User else {
                            fatalError("Couldn't get user")
                        }
                        
                        self.user = userData
                        print(userData)
                    
                    }catch{
                        fatalError("Couldn't encode data")
                    }
                   
                }
                
            }) { (error) in
                print("Error: {0}", error)
            }
        }
    }

    fileprivate let session: WCSession? = WCSession.isSupported() ? WCSession.default : nil
    
    var user: User?
    override init() {
        super.init()
        session?.delegate = self
        session?.activate()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
     
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
    
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
